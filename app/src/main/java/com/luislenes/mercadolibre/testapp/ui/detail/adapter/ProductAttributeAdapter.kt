package com.luislenes.mercadolibre.testapp.ui.detail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.luislenes.mercadolibre.domain.ProductAttribute
import com.luislenes.mercadolibre.testapp.databinding.ListItemProductAttrBinding

class ProductAttributeAdapter(
    private val attributes: MutableList<ProductAttribute> = mutableListOf()
) : RecyclerView.Adapter<ProductAttributeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductAttributeViewHolder {
        return ProductAttributeViewHolder(
            ListItemProductAttrBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ProductAttributeViewHolder, position: Int) {
        holder.bind(attributes[position])
    }

    override fun getItemCount(): Int = attributes.size
}