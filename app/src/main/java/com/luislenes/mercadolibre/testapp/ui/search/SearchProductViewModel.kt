package com.luislenes.mercadolibre.testapp.ui.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.luislenes.mercadolibre.domain.Product
import com.luislenes.mercadolibre.interactors.SearchProducts
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class SearchProductViewModel(private val searchProducts: SearchProducts) : ViewModel() {

    val products: MutableLiveData<List<Product>> = MutableLiveData()

    fun searchProductsByName(query: String?) {
        GlobalScope.launch(Dispatchers.IO) {
            products.postValue(query?.run { searchProducts(query) } ?: listOf())
        }
    }
}