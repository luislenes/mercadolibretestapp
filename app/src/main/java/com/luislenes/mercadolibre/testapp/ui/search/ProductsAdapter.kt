package com.luislenes.mercadolibre.testapp.ui.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.luislenes.mercadolibre.domain.Product
import com.luislenes.mercadolibre.testapp.databinding.ListItemProductBinding
import com.luislenes.mercadolibre.testapp.ui.detail.ProductItem
import com.luislenes.mercadolibre.testapp.ui.util.GlideApp

class ProductsAdapter(
    private val products: MutableList<Product>,
    private val onClickItem: (product: ProductItem) -> Unit
) : RecyclerView.Adapter<ProductsAdapter.ProductViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        return ProductViewHolder(
            ListItemProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        products[position].run {
            holder.bind(this)
            holder.itemView.setOnClickListener {
                onClickItem(ProductItem(id, name, status, attributes, pictures))
            }
        }
    }

    override fun getItemCount(): Int = products.size

    fun updateProducts(list: List<Product>) {
        products.clear()
        products.addAll(list)
        notifyDataSetChanged()
    }

    class ProductViewHolder(
        private val binding: ListItemProductBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(product: Product) {
            binding.tvProductName.text = product.name
            GlideApp.with(itemView.context)
                .load(product.pictures.first().url)
                .into(binding.ivProductPicture)
        }
    }
}