package com.luislenes.mercadolibre.testapp.ui.detail

import android.os.Parcelable
import com.luislenes.mercadolibre.domain.ProductAttribute
import com.luislenes.mercadolibre.domain.ProductPicture
import com.luislenes.mercadolibre.util.StringUtil.EMPTY
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue

@Parcelize
class ProductItem(
    val id: String = EMPTY,
    val name: String = EMPTY,
    val status: String = EMPTY,
    val attributes: @RawValue List<ProductAttribute> = listOf(),
    val pictures: @RawValue List<ProductPicture> = listOf()
) : Parcelable