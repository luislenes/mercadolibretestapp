package com.luislenes.mercadolibre.testapp.ui.detail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.luislenes.mercadolibre.domain.ProductPicture
import com.luislenes.mercadolibre.testapp.databinding.ListItemProductPicBinding

class ProductPictureAdapter(
    private val pictures: MutableList<ProductPicture> = mutableListOf()
) : RecyclerView.Adapter<ProductPictureViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductPictureViewHolder {
        return ProductPictureViewHolder(
            ListItemProductPicBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ProductPictureViewHolder, position: Int) {
        holder.bind(pictures[position])
    }

    override fun getItemCount(): Int = pictures.size

}