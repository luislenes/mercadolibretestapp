package com.luislenes.mercadolibre.testapp.di

import com.luislenes.mercadolibre.data.ProductDataSource
import com.luislenes.mercadolibre.data.ProductRepository
import com.luislenes.mercadolibre.interactors.SearchProducts
import com.luislenes.mercadolibre.testapp.framework.DummyProductDataSource
import com.luislenes.mercadolibre.testapp.ui.search.SearchProductViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val coreModule = module {
    single<ProductDataSource> { DummyProductDataSource(androidContext()) }
    single { ProductRepository(get()) }
    single { SearchProducts(get()) }
}

val viewModelModule = module {
    viewModel { SearchProductViewModel(get()) }
}