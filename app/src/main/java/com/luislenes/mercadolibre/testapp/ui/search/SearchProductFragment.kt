package com.luislenes.mercadolibre.testapp.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import com.luislenes.mercadolibre.testapp.R
import com.luislenes.mercadolibre.testapp.databinding.FragmentSearchProductBinding
import com.luislenes.mercadolibre.testapp.ui.util.setVisibility
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchProductFragment : Fragment() {

    private val viewModel by viewModel<SearchProductViewModel>()
    private lateinit var binding: FragmentSearchProductBinding
    private lateinit var productsAdapter: ProductsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchProductBinding.inflate(inflater, container, false)
        setUpRecyclerView()
        setUpToolbar()

        return binding.root
    }

    private fun setUpRecyclerView() {
        binding.rvProductListResult.run {
            layoutManager = GridLayoutManager(context, 2, VERTICAL, false)
            productsAdapter = ProductsAdapter(mutableListOf()) {
                findNavController().navigate(
                    SearchProductFragmentDirections.actionSeeProductDetail(it)
                )
            }
            adapter = productsAdapter

            viewModel.products.observe(viewLifecycleOwner) {
                setVisibility(it.isNotEmpty())
                binding.tvSearchMessage.setVisibility(it.isEmpty())
                binding.tvSearchMessage.setText(R.string.empty_search_message)
                productsAdapter.updateProducts(it)
            }
        }
    }

    private fun setUpToolbar() {
        binding.toolbarContainer.toolbarSearch.run {
            setOnQueryTextListener(searchViewListener)
        }
    }

    private val searchViewListener = object : SearchView.OnQueryTextListener {

        override fun onQueryTextSubmit(query: String?): Boolean {
            viewModel.searchProductsByName(query)
            return true
        }

        override fun onQueryTextChange(newText: String): Boolean {
            if (newText.isEmpty()) {
                binding.tvSearchMessage.setText(R.string.initial_search_message)
            }
            return true
        }
    }
}