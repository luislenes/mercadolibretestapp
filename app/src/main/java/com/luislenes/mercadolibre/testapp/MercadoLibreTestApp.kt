package com.luislenes.mercadolibre.testapp

import android.app.Application
import com.luislenes.mercadolibre.testapp.di.coreModule
import com.luislenes.mercadolibre.testapp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MercadoLibreTestApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@MercadoLibreTestApp)
            modules(coreModule, viewModelModule)
        }
    }
}