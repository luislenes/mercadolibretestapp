package com.luislenes.mercadolibre.testapp.ui.detail.adapter

import androidx.recyclerview.widget.RecyclerView
import com.luislenes.mercadolibre.domain.ProductPicture
import com.luislenes.mercadolibre.testapp.databinding.ListItemProductPicBinding
import com.luislenes.mercadolibre.testapp.ui.util.GlideApp

class ProductPictureViewHolder(
    private val binding: ListItemProductPicBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(productPic: ProductPicture) {
        GlideApp.with(itemView.context)
            .load(productPic.url)
            .into(binding.ivProductPicture)
    }
}