package com.luislenes.mercadolibre.testapp.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.luislenes.mercadolibre.testapp.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}