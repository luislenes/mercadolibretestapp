package com.luislenes.mercadolibre.testapp.ui.util

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE

fun View.setVisibility(visible: Boolean) {
    visibility = if (visible) VISIBLE else GONE
}