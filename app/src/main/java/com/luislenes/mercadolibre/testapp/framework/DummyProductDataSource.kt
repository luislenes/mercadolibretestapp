package com.luislenes.mercadolibre.testapp.framework

import android.content.Context
import com.google.gson.Gson
import com.luislenes.mercadolibre.data.ProductDataSource
import com.luislenes.mercadolibre.domain.ProductRequest
import com.luislenes.mercadolibre.testapp.R
import java.io.BufferedReader
import java.io.InputStreamReader

class DummyProductDataSource(context: Context) : ProductDataSource {

    private var dummyInfo: ProductRequest =
        context.resources.openRawResource(R.raw.products_search_dummy_result).let {
            Gson().fromJson(
                BufferedReader(InputStreamReader(it)).use { buffer -> buffer.readLine() },
                ProductRequest::class.java
            )
        }

    override suspend fun search(query: String): ProductRequest =
        dummyInfo.copy(results = dummyInfo.results.filter {
            it.name.contains(query, true)
        })
}