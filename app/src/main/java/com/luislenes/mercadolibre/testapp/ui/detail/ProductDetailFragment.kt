package com.luislenes.mercadolibre.testapp.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import com.luislenes.mercadolibre.testapp.R
import com.luislenes.mercadolibre.testapp.databinding.FragmentProductDetailBinding
import com.luislenes.mercadolibre.testapp.ui.detail.adapter.ProductAttributeAdapter
import com.luislenes.mercadolibre.testapp.ui.detail.adapter.ProductPictureAdapter

class ProductDetailFragment : Fragment() {

    private lateinit var binding: FragmentProductDetailBinding
    private val args: ProductDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProductDetailBinding.inflate(inflater, container, false)
        binding.tvProductId.text = args.product.id
        binding.tvProductName.text = args.product.name
        binding.tvProductStatus.text = args.product.status

        setUpToolbar()
        setUpRecyclerViews()
        return binding.root
    }

    private fun setUpToolbar() {
        binding.toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        binding.toolbar.setNavigationOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun setUpRecyclerViews() {
        binding.rvProductPictures.run {
            layoutManager = LinearLayoutManager(context, HORIZONTAL, false)
            adapter = ProductPictureAdapter(args.product.pictures.toMutableList())
        }
        binding.rvProductAttributes.run {
            layoutManager = LinearLayoutManager(context, VERTICAL, false)
            adapter = ProductAttributeAdapter(args.product.attributes.toMutableList())
        }
    }
}