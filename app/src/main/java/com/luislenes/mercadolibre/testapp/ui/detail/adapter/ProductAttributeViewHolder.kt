package com.luislenes.mercadolibre.testapp.ui.detail.adapter

import androidx.recyclerview.widget.RecyclerView
import com.luislenes.mercadolibre.domain.ProductAttribute
import com.luislenes.mercadolibre.testapp.R
import com.luislenes.mercadolibre.testapp.databinding.ListItemProductAttrBinding

class ProductAttributeViewHolder(
    private val binding: ListItemProductAttrBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(productAttr: ProductAttribute) {
        binding.tvProductAttr.text = itemView.context
            .getString(R.string.product_attribute_template, productAttr.name, productAttr.valueName)
    }
}