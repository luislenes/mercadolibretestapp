package com.luislenes.mercadolibre.domain

import com.luislenes.mercadolibre.util.StringUtil.EMPTY

data class ProductRequest(
    val keywords: String = EMPTY,
    val paging: ProductPaging = ProductPaging(),
    val results: List<Product> = listOf()
)