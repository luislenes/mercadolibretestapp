package com.luislenes.mercadolibre.domain

import com.luislenes.mercadolibre.util.StringUtil.EMPTY

data class ProductPicture(
    val id: String = EMPTY,
    val url: String = EMPTY
)