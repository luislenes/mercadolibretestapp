package com.luislenes.mercadolibre.domain

import com.google.gson.annotations.SerializedName
import com.luislenes.mercadolibre.util.StringUtil.EMPTY

data class ProductAttribute(
    val id: String = EMPTY,
    val name: String = EMPTY,
    @SerializedName("value_id") val valueId: String = EMPTY,
    @SerializedName("value_name") val valueName: String = EMPTY
)