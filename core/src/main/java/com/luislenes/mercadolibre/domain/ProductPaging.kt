package com.luislenes.mercadolibre.domain

data class ProductPaging(
    val total: Int = 0,
    val limit: Int = 0,
    val offset: Int = 0
)