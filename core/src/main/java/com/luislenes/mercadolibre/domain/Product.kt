package com.luislenes.mercadolibre.domain

import com.luislenes.mercadolibre.util.StringUtil.EMPTY

data class Product(
    val id: String = EMPTY,
    val name: String = EMPTY,
    val status: String = EMPTY,
    val attributes: List<ProductAttribute> = listOf(),
    val pictures: List<ProductPicture> = listOf()
)