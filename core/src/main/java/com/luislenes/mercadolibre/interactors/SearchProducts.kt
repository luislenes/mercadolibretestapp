package com.luislenes.mercadolibre.interactors

import com.luislenes.mercadolibre.data.ProductRepository

class SearchProducts(private val productRepository: ProductRepository) {

    suspend operator fun invoke(query: String) = productRepository.searchProducts(query)

}