package com.luislenes.mercadolibre.data

import com.luislenes.mercadolibre.domain.ProductRequest

interface ProductDataSource {

    suspend fun search(query: String): ProductRequest

}