package com.luislenes.mercadolibre.data

import com.luislenes.mercadolibre.domain.Product

class ProductRepository(private val dataSource: ProductDataSource) {

    suspend fun searchProducts(query: String): List<Product> = dataSource.search(query).results

}